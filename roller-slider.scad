// The effective diameter of the roller, that is excluding that part that sinks into the slot (in mm)
roller_effective_diameter = 22;
// The roller inner diameter, which is supposed to coincide with the screw diameter (in mm)
roller_inner_diameter = 5;
// The roller thickness, so that we can center it on the rail (in mm)
roller_thickness = 11;

/* [Hidden] */
layer_height=.2; extrusion_width=.5;
epsilon=.01;

module roller_slider(
 rail_w=20,                     // rail width, that is 20 for 2020 extrusion
 roller_h = 11,                 // roller width
 roller_od = 24,                // roller outer diameter, irrelevant, actually
 roller_d = 22,                 // roller effective diameter
 roller_id = 5,                 // roller inner diameter (i.e. screw diameter)
 roller_iod = 8.3,              // roller inner ring diameter (the support tower top diameter)
 thickness = 7,                 // slider thickness
 gap = 0.5,                     // gap between the rail and the slider
 play = 0.1,                    // play on each side when not tightened
 nut_d = 5.4/cos(30),           // carriage mount screw dimensions
 nut_h = 3.1,
 screw_d = 3,
 sink_flat = 1.5,               // sink the big screw further this far
 l = 66,                        // slider length (height)
 arc_w = 10,                    // lever arc width
 rail_clearance=1.5,            // basically a distance between the spacer tower and the extrusion
 tighten_travel=4.5,            // how far can we travel for tightening
 tab_w=6,                       // lever tab width
 screw_shell=6                  // minimum shell around the screw
) {
 fnd=2*PI; fnr=2*fnd;
 long_pattern = [3.8*2,28.5*2];
 short_pattern = [20,20];
 sideways = rail_w/2 + roller_d/2 + play;
 base_d = roller_d-2*rail_clearance;
 roller_span = l-base_d;
 roller_z = thickness+rail_w/2-roller_h/2 + gap;
 arc_or = sideways+base_d/2-rail_w/2;
 tighten_y = max(base_d/2,short_pattern.y/2+nut_d/2)+screw_shell;
 module vitamins() {
  translate([0,0,thickness+epsilon])
  rotate([90,0,0])
  translate([-rail_w/2,0,-l/2-1])
  linear_extrude(height=l+2,convexity=8)
  square([rail_w,rail_w]);
 }//vitamins module
 module screwhole(what) {
  zr = 30;
  if(what=="out") {
   translate([0,0,-1])
   cylinder(d=screw_d,h=thickness+2,$fn=screw_d*fnd);
   translate([0,0,thickness-nut_h]) rotate([0,0,zr])
   cylinder(d=nut_d,h=nut_h+1,$fn=6);
  }else if(what=="in") {
   rotate([0,0,zr])
   cylinder(d=nut_d,h=thickness-nut_h,$fn=6);
  }
 }//screwhole module
 module screwpattern(pattern,what) {
  for(sx=[-1,1]) for(sy=[-1,1])
   translate([sx*pattern.x/2,sy*pattern.y/2,0]) screwhole(what=what);
 }//screwpattern module
 module screwpatterns(what) {
  screwpattern(pattern=long_pattern,what=what);
  screwpattern(pattern=short_pattern,what=what);
 }//screwpatterns module
 module rolltower(what) {
  if(what=="base")
   cylinder(d=base_d,h=thickness,$fn=base_d*fnd);
  else if(what=="in") {
    translate([0,0,thickness-epsilon])
    cylinder(d1=base_d,d2=roller_iod,h=roller_z-thickness+epsilon,$fn=base_d*fnd);
  }else if(what=="out") {
   translate([0,0,-1])
   cylinder(d=roller_id,h=roller_z+2,$fn=roller_id*fnd);
   translate([0,0,sink_flat])
   cylinder(d1=roller_id*2,d2=roller_id,h=roller_id/2,$fn=roller_id*2*fnd);
   translate([0,0,-epsilon])
   cylinder(d=roller_id*2,h=sink_flat+2*epsilon,$fn=roller_id*2*fnd);
  }else if(what=="baseout") {
   translate([0,0,-1])
   cylinder(d=base_d,h=thickness+2,$fn=base_d*fnd);
  }
 }//rolltower module
 module placetowers() {
  translate([-sideways,+roller_span/2,0]) children();
  translate([+sideways,             0,0]) children();
  translate([-sideways,-roller_span/2,0]) children();
 }//placetowers module
 
 % vitamins();
 difference() {
  union() {
   difference() {
    // base plate
    translate([-rail_w/2,-l/2,0])
    cube(size=[rail_w,l,thickness]);
    translate([+sideways-tighten_travel,0,0]) rolltower(what="baseout");
   }
   // screws
   screwpatterns(what="in");
   // tower base are belong to us
   for(my=[0,1]) mirror([0,my,0]) hull() {
    translate([-sideways,roller_span/2,0]) rolltower(what="base");
    basebase=l/2-short_pattern.y/2+nut_d/2+2*extrusion_width;
    translate([-rail_w/2,l/2-basebase,0]) cube(size=[rail_w/2,basebase,thickness]);
   }
   hull() {
    translate([rail_w/2+arc_or-arc_w,-l/2+arc_or-epsilon,0])
    cube(size=[arc_w,l/2-arc_or+epsilon,thickness]);
    translate([+sideways,             0,0]) rolltower(what="base");
   }
   // towering genii
   placetowers() rolltower(what="in");
   // arc
   difference() {
    translate([rail_w/2,-l/2+arc_or,0])
    cylinder(r=arc_or,h=thickness,$fn=arc_or*fnr);
    translate([rail_w/2,-l/2+arc_or,-1])
    cylinder(r=arc_or-arc_w,h=thickness+2,$fn=arc_or*fnr);
    translate([rail_w/2-arc_or-1,-l/2+arc_or,-1])
    cube(size=[arc_or*2+2,arc_or+1,thickness+2]);
   }
   // tab
   translate([sideways+base_d/2-tab_w,-base_d/2,0])
   cube(size=[tab_w,base_d/2+tighten_y+screw_shell,thickness]);
   // tightener
   translate([rail_w/2-epsilon,tighten_y-screw_shell,0])
   cube(size=[sideways+base_d/2-tab_w-tighten_travel-rail_w/2+epsilon,screw_shell*2,thickness]);
  }//union
  // screw it!
  screwpatterns(what="out");
  // big roller screwholes
  placetowers() rolltower(what="out");
  // tightener screwhole
  translate([0,tighten_y,thickness/2]) rotate([0,90,0])
  cylinder(d=screw_d,h=sideways+base_d/2+1,$fn=screw_d*fnd);
  // tightener nut pocke
  hull() for(z=[thickness/2,thickness+nut_d])
  translate([rail_w/2 + (sideways+base_d/2-tab_w-tighten_travel-rail_w/2)/2,tighten_y,z])
  rotate([0,90,0])
  cylinder(d=nut_d,h=nut_h,center=true,$fn=6);
 }//difference
 
}//roller_slider module

roller_slider(
 roller_d  = roller_effective_diameter,
 roller_id = roller_inner_diameter,
 roller_h  = roller_thickness
);

/* The design (not the code) is based on https://www.thingiverse.com/thing:1524798/ */
