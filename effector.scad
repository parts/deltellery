layer_height=.2; extrusion_width=.5;
epsilon=.01;

module effector(
 rods_span = 40,                                // separation between pushrods
 rodmount_l = 7,                                // the thickness of tod mount
                                                // (basically the length of the part of the screw
                                                // going through the mount).
 rodmount_screw_l=20, rodmount_screw_d=3.2,     // rod mount screw dimensions
 rodmount_screwhead_d=6, rodmount_screwhead_h=3,
 rodmount_screw_clearance = 1,                  // this is where they meet
 rodmount_nut_w = 5.5, rodmount_nut_h = 3.1,    // rod mount nuts dimensions
 rodmount_d = 5,                                // the diameter of the traxxas thingie base
 rodmount_angle = 45,                           // angle of the rodmount
 rodmount_clearance = 6,                        // clearance for traxxas thingies

 heatsink_d = 22.3,                             // heatsink dimensions
 heatsink_clearance = 1,
 heatsink_groove_d = 12, heatsink_groove_h = 6,
 heatsink_ungroove_d = 16, heatsink_ungroove_h=3.7,

 platform_screw_d=3.2,                          // the dimensions of screws and nuts pockets
 platform_nut_w = 5.5, platform_nut_h = 2.5,      // all over the place
 topholder_screw_d = 3.1, topholder_screw_l = 10,
 topholder_screwhead_h = 3, topholder_screwhead_d = 6,
 spring_l1 = 19, spring_l0 = 6, spring_d = 5,   // spring dimensions
 
 top_overlap = 3*extrusion_width,               // overlap of the semistatic circle
                                                // and heatsink hole edges
 
 snap_magnet_d = 10, snap_magnet_h = 4,
 snap_guide_w = extrusion_width*5, snap_guide_h = 10,
 // connect fan on the effector side
 fan_screw_d = 3.1, fan_screw_l = 10,
 fan_screwhead_d = 6, fan_screwhead_h = 3,
 fan_wire_hole_d = 3,
 fan_link_w = 3, fan_link_l = 10,
 fanlink_screw_d = 3.1,
 fanlink_screwhead_d = 3, fanlink_nut_w = 5.5,
 fanlink_mount_clearance = 1,
 snap_rods_extra_clearance = 2.5,
 
 heatsink_groove_d_tolerance = 0.3, heatsink_ungroove_d_tolerance = 0.3,
 snap_magnet_d_tolerance = 0.3,
 snap_guide_w_tolerance = extrusion_width, snap_guide_h_tolerance = layer_height,
 what = "*",

 minvs = 2*extrusion_width,                     // minimal vertical shell
 vs = 4*extrusion_width,                        // tolerable vertical shell
 minhs = 2*layer_height,
 hs = 4*layer_height,
) {
 fnd = PI*2; fnr = fnd*2; sfnr = fnr*2;
 
 top_raise = 1;

 maxs = max(vs,hs);
 maxmins = max(minvs,minhs);

 rodmount_nut_d = rodmount_nut_w/cos(30);
 platform_nut_d = platform_nut_w/cos(30);

 hshole_d = heatsink_d+2*heatsink_clearance;    // heatsink hole diameter

 function rodmount_r_for_inter_r(r) = sqrt(
  4*pow(rodmount_clearance,2) + 8*rodmount_clearance*r + 4*pow(r,2) - pow(rods_span,2)
 )/2;
 // distance to the rodmount line
 rodmount_r = let(extra=hshole_d/2+rodmount_nut_d/2) max(
                   // pocketed nuts:
                   extra + platform_nut_d,
                   // clearance for rod mount screws
                   (rods_span/2+rodmount_l+rodmount_screwhead_h)/tan(60)+rodmount_screw_clearance
 );
 echo("rodmount_r",rodmount_r);
 effector_h = max( rodmount_screw_d+2*minhs,
                   rodmount_nut_w  +2*layer_height,
                   platform_nut_h  +2*minhs,
                   rodmount_d );
 echo("effector_h",effector_h);

 rodmount_w = rodmount_screw_l-rodmount_l;
 pw2 = sqrt(pow(rods_span/2,2)+pow(rodmount_r,2))-rodmount_clearance; // platform width/2
 pl2 = rodmount_r+rodmount_nut_d/2+minvs;       // platform length/2
 hotend_holder_h = heatsink_groove_h+heatsink_ungroove_h;
 echo("hotend_holder_h",hotend_holder_h);

 snap_guide_l = snap_magnet_h+maxs;
 
 module rodmount(which) {
  w = rodmount_w;
  d2 = effector_h+2*w*tan(rodmount_angle);
  if(which=="in") {
   intersection() {
    for(mx=[0,1]) mirror([mx,0,0])
     translate([-rods_span/2,rodmount_r,0]) rotate([0,90,0])
     cylinder(d1=rodmount_d,d2=d2,h=w,$fn=d2*fnd);
    cube(size=[rods_span+2,2*pl2,effector_h],center=true);
   }
  }else if(which=="out") {
   translate([0,rodmount_r,0]) rotate([0,90,0])
   cylinder(d=rodmount_screw_d,h=rods_span+2,center=true,$fn=rodmount_screw_d*fnd);
   translate([-rods_span/2+w+epsilon,rodmount_r-rodmount_nut_d/2,-effector_h/2-1])
   cube(size=[rods_span-2*w-2*epsilon,d2+1,effector_h+2]);
   for(mx=[0,1]) mirror([mx,0,0])
    translate([rods_span/2-w-1,rodmount_r,0])
    rotate([0,90,0])
    rotate([0,0,30])
    cylinder(d=rodmount_nut_d,h=rodmount_nut_h+1,$fn=6);
  }//if
 }//rodmount module

 module rodmounts(which) {
  for(zr=[0:360/3:359]) rotate([0,0,zr])
   rodmount(which);
 }
 
 module platform(which) {
  if(which=="in") {
   pr = pw2/cos(30); // platform radius
   cylinder(d=2*pr,h=effector_h,center=true,$fn=6);
  }else if(which=="out") {
   cylinder(d=hshole_d,h=effector_h+2,center=true,$fn=hshole_d*fnd);
  }
 }

 module static() {
  module hole_in_my_pocket(wheres,
    nut_h=platform_nut_h,nut_w=platform_nut_w,screw_d=platform_screw_d,
    nut_d=platform_nut_d,
    outer_r,offset=0) {
   midway = (outer_r+hshole_d/2)/2;
   for(zr=wheres) rotate([0,0,zr]) {
    if(!offset)
     translate([0,midway,0])
     cube(size=[nut_w,outer_r-hshole_d/2+2,nut_h],center=true);
    else
     hull() for(y=[offset,outer_r+nut_w/2])
     translate([0,y,0])
     cylinder(d=nut_d,h=nut_h,center=true,$fn=6);
    translate([0,(offset>0)?offset:midway,0])
    for(tmz=[[nut_h/2+layer_height,0],[nut_h/2,1]])
     translate([0,0,tmz[0]]) mirror([0,0,tmz[1]])
     cylinder(d=screw_d,h=effector_h+2,$fn=screw_d*fnd);
   }
  }
  difference() {
   union() {
    rodmounts(which="in");
    platform(which="in");
   }//union
   rodmounts(which="out");
   platform(which="out");
   // pockets and holes
   hole_in_my_pocket(wheres=[-60,60,180], outer_r=pw2);
   hole_in_my_pocket(wheres=[-120,120,0], outer_r=rodmount_r-rodmount_nut_d/2);
  }//difference
 }

 module semistatic() {
  od=heatsink_ungroove_d+heatsink_ungroove_d_tolerance+2*vs;
  hhh = hotend_holder_h;
  smooth_r = 1.5;
  youter = (rodmount_r-rodmount_nut_d/2);
  yscrew = (youter+hshole_d/2)/2;
  d_scroo = (youter-yscrew)*2;
  z_scroohead = topholder_screw_l-effector_h;
  translate([0,0,effector_h/2]) {
   difference() {
    hull() {
     // outer body
     rotate_extrude($fn=od*fnd) {
      square([od/2-smooth_r,hhh]);
      square([od/2,hhh-smooth_r]);
      translate([od/2-smooth_r,hhh-smooth_r])
      circle(r=smooth_r,$fn=smooth_r*sfnr);
     }
     cylinder(d=hshole_d+2*top_overlap,h=hs,$fn=(hshole_d+2*top_overlap)*fnd);
     for(zr=[0:120:359]) rotate([0,0,zr])
     translate([0,yscrew,0])
     cylinder(d=d_scroo,h=z_scroohead,$fn=d_scroo*fnd);
    }//union
    // top screws
    for(zr=[0:120:359]) rotate([0,0,zr])
    translate([0,yscrew,0]) {
     translate([0,0,-1])
     cylinder(d=topholder_screw_d,h=hhh+2,$fn=topholder_screw_d*fnd);
     translate([0,0,topholder_screw_l-effector_h])
     cylinder(d=topholder_screwhead_d,h=hhh,$fn=topholder_screwhead_d*fnd);
    }
    fnarrowing = 0.95;
    // hotend narrow
    translate([0,0,-1])
    cylinder(d=heatsink_groove_d+heatsink_groove_d_tolerance,h=hhh+2,$fn=heatsink_groove_d*fnd);
    hull() for(y=[0,-od/2-heatsink_groove_d/2-1])
    translate([0,y,-1])
    cylinder(d=fnarrowing*(heatsink_groove_d+heatsink_groove_d_tolerance),h=hhh+2,$fn=heatsink_groove_d*fnd);
    // hotend wide
    translate([0,0,heatsink_groove_h])
    cylinder(d=heatsink_ungroove_d+heatsink_ungroove_d_tolerance,h=heatsink_ungroove_h+1,$fn=heatsink_ungroove_d*fnd);
    hull() for(y=[0,-od/2-heatsink_ungroove_d/2-1])
    translate([0,y,heatsink_groove_h])
    cylinder(d=fnarrowing*(heatsink_ungroove_d+heatsink_ungroove_d_tolerance),h=heatsink_ungroove_h+1,$fn=heatsink_ungroove_d*fnd);
   }//difference
  }//translate
 }//semistatic module

 fanlink_mount_d = max(fanlink_screwhead_d,fanlink_nut_w/cos(30));
 fansnap_screw_to_magnets = fan_screwhead_d/2+maxs+vs;
 fansnap_magnets_z = max(effector_h/2,rodmount_clearance)+snap_rods_extra_clearance+maxs+snap_magnet_d/2;
 fansnap_intermagnets = fan_screwhead_d+2*maxs;
 module fan_effector() {
  rotate([0,0,60]) translate([0,(hshole_d/2+pw2)/2,0]) {
   difference() {
    hull() {
     translate([0,0,-effector_h/2]) mirror([0,0,1])
     cylinder(d=fan_screwhead_d+2*maxs,h=fan_screw_l-effector_h/2,$fn=(fan_screwhead_d+2*maxs)*fnd);
     for(mx=[0,1]) mirror([mx,0,0]) {
      translate([(fansnap_intermagnets+snap_magnet_d)/2,
                 fan_screwhead_d/2,
                 -fansnap_magnets_z])
      rotate([-90,0,0])
      cylinder(d=snap_magnet_d+snap_magnet_d_tolerance+2*maxs,h=snap_magnet_h+maxs+vs,$fn=(snap_magnet_d+snap_magnet_d_tolerance+2*maxs)*fnd);
      translate([fansnap_intermagnets/2-fan_screw_d/2,pw2-(hshole_d/2+pw2)/2-fan_screw_d/2,-effector_h/2])
      mirror([0,0,1]) cylinder(d=fan_screw_d,h=epsilon,$fn=fan_screw_d*fnd);
     }
    }//hull
    for(mx=[0,1]) mirror([mx,0,0])
     translate([(fansnap_intermagnets+snap_magnet_d)/2,
                fansnap_screw_to_magnets,
                -fansnap_magnets_z])
     rotate([-90,0,0]) {
      cylinder(d=snap_magnet_d+snap_magnet_d_tolerance,h=snap_magnet_h+1,$fn=(snap_magnet_d+snap_magnet_d_tolerance)*fnd);
      mirror([0,0,1]) translate([0,0,-1])
      cylinder(d=fan_wire_hole_d,h=fansnap_screw_to_magnets*2+1,$fn=fan_wire_hole_d*fnd);
     }//rotate
    translate([0,0,-effector_h/2-(fan_screw_l-effector_h/2-platform_nut_h/2)])
    mirror([0,0,1])
    cylinder(d=fan_screwhead_d,h=fansnap_magnets_z*2+1,$fn=fan_screwhead_d*fnd);
    translate([0,0,-fansnap_magnets_z])
    hull() for(sz=[-1,1]) translate([0,0,sz*(snap_guide_h+snap_guide_h_tolerance-snap_guide_w-snap_guide_w_tolerance)/2])
    rotate([-90,0,0])
    cylinder(d=snap_guide_w+snap_guide_w_tolerance,h=maxs+fan_screwhead_d+maxs+snap_magnet_h+2,$fn=(snap_guide_w+snap_guide_w_tolerance)*fnd);
    mirror([0,0,1]) translate([0,0,-1])
    cylinder(d=fan_screw_d,h=fan_screw_l+2,$fn=fan_screw_d*fnd);
   }//difference
  }//translate rotate
 }//fan_effector module
 
 module fan_fan() {
  rotate([0,0,60]) translate([0,(hshole_d/2+pw2)/2 + fansnap_screw_to_magnets+snap_magnet_h+1,0]) {
   difference() {
    union() {
     hull() {
      for(mx=[0,1]) mirror([mx,0,0])
      translate([(fansnap_intermagnets+snap_magnet_d)/2,0,-fansnap_magnets_z])
      rotate([-90,0,0])
      cylinder(d=snap_magnet_d+snap_magnet_d_tolerance+2*maxs,h=snap_magnet_h+vs+maxs,$fn=(snap_magnet_d+snap_magnet_d_tolerance+maxs)*fnd);
      translate([-fan_link_w/2,0,-fansnap_magnets_z-(snap_magnet_d+snap_magnet_d_tolerance)/2-maxs])
      cube(size=[fan_link_w,snap_magnet_h+vs+maxs+fan_link_l-fanlink_mount_d-fanlink_mount_clearance,fanlink_mount_d]);
     }//hull
     hull()
     translate([0,0,-fansnap_magnets_z])
     for(sz=[-1,0,1]) translate([0,
                                 sz?0: -(snap_guide_l-snap_guide_w/2),
                                 sz*(snap_guide_h-snap_guide_w)/2])
     sphere(d=snap_guide_w,$fn=snap_guide_w*fnd*2);
     translate([-fan_link_w/2,0,-fansnap_magnets_z-(snap_magnet_d+snap_magnet_d_tolerance)/2-maxs])
     cube(size=[fan_link_w,snap_magnet_h+vs+maxs+fan_link_l-fanlink_mount_d/2,fanlink_mount_d]);
     hull() {
      translate([0,snap_magnet_h+vs+maxs+fan_link_l-fanlink_mount_d/2,-fansnap_magnets_z-(snap_magnet_d+snap_magnet_d_tolerance)/2-maxs+fanlink_mount_d/2])
      rotate([0,90,0])
      cylinder(d=fanlink_mount_d,h=fan_link_w,center=true,$fn=fanlink_mount_d*fnd);
      translate([-fan_link_w/2,0,-fansnap_magnets_z-(snap_magnet_d+snap_magnet_d_tolerance)/2-maxs])
      cube(size=[fan_link_w,snap_magnet_h+vs+maxs,snap_magnet_d+snap_magnet_d_tolerance+2*maxs]);
     }
    }//union
    for(mx=[0,1]) mirror([mx,0,0])
    translate([(fansnap_intermagnets+snap_magnet_d)/2,-epsilon,-fansnap_magnets_z])
    rotate([-90,0,0]) {
     cylinder(d=snap_magnet_d+snap_magnet_d_tolerance,h=snap_magnet_h,$fn=(snap_magnet_d+snap_magnet_d_tolerance)*fnd);
     cylinder(d=fan_wire_hole_d,h=snap_magnet_h+vs+maxs+fan_link_l,$fn=fan_wire_hole_d*fnd);
    }
    translate([0,snap_magnet_h+vs+maxs+fan_link_l-fanlink_mount_d/2,-fansnap_magnets_z-(snap_magnet_d+snap_magnet_d_tolerance)/2-maxs+fanlink_mount_d/2])
    rotate([0,90,0])
    cylinder(d=fanlink_screw_d,h=fan_link_w+2,center=true,$fn=fanlink_screw_d*fnd);
   }//difference
  }//translate rotate
 }//fan_fan module
 
 module vitamins() {
  // fan snapper's screw & magnets
  rotate([0,0,60]) translate([0,(hshole_d/2+pw2)/2,0]) {
   translate([0,0,-fan_screw_l+effector_h/4]) {
    cylinder(d=fan_screw_d,h=fan_screw_l,$fn=fan_screw_d*fnd);
    mirror([0,0,1]) cylinder(d=fan_screwhead_d,h=fan_screwhead_h,$fn=fan_screwhead_d*fnd);
   }
   for(oy=[0,snap_magnet_h+1]) translate([0,oy,0])
   for(mx=[0,1]) mirror([mx,0,0])
   translate([(fansnap_intermagnets+snap_magnet_d)/2,
              fansnap_screw_to_magnets,
              -fansnap_magnets_z])
   rotate([-90,0,0])
    cylinder(d=snap_magnet_d,h=snap_magnet_h+epsilon,$fn=snap_magnet_d*fnd);
  }
  // rodmount
  for(rz=[0:120:359]) rotate([0,0,rz])
  for(mx=[0,1]) mirror([mx,0,0])
  translate([rods_span/2,rodmount_r,0])
  rotate([0,90,0]) {
   cylinder(d=2*rodmount_clearance,h=rodmount_l);
   cylinder(d=rodmount_screwhead_d,h=rodmount_l+rodmount_screwhead_h);
  }
 }

% vitamins();

 if(what=="static") {
  static();
 }else if(what=="semistatic") {
  semistatic();
 }else if(what=="fan:effector") {
  fan_effector();
 }else if(what=="fan:fan") {
  fan_fan();
 }else{
  color("green",0.8) static();
  translate([0,0,top_raise+epsilon])
  color("yellow",0.8) semistatic();
  color("orange",0.8) fan_effector();
  color("red",0.8) fan_fan();
 }

}

what="*";
effector(what=what);
/* vim:set ts=8 sw=1 expandtab: */
