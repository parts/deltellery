-include Makefile.local

OPENSCAD_BIN?=openscad
OPENSCAD_FLAGS=-D draft=false

default:
	@echo "And?"

clean:
	rm -f *.stl *.gcode *.deps

vertex: $(addsuffix .stl,$(addprefix vertex-,motor idler idler-tensioner))

effector: $(addsuffix .stl,$(addprefix effector-,static semistatic))

%.stl: %.scad
	$(OPENSCAD_BIN) $(OPENSCAD_FLAGS) -m make -d $@.deps -o "$@" "$<"

-include $(wildcard *.deps)
