layer_height=.2; extrusion_width=.5;
epsilon=.01;

module hallmount(
 pcb_size = [23,23], pcb_mountholes_offset=1.2+3/2,
 pcb_mountscrew_d = 1.5,
 sensor_offset = 2.5, // sensor offset from the edge of pcb
 sensor_protrusion = 5+1, // how far away from pcb 
 ew = 20, // 2020 extrusion width
 sw = 6, // slot width
 sd = 6, // slot_depth
 tnut_l = 10,
 screw_d = 3.1,
 screw_hd = 6, screw_hh = 3,
 screw_l = 6,
 magnet_offset = 22/2,
 clearance = 1,
 s = 2,
 standoff_h = 2,
) {
 fnd=PI*2; fnr=fnd*2;
 
 module vitamins() {
  // extrusion
  difference() {
   translate([-ew/2,0,-3*ew/2])
   cube(size=[ew,ew,3*ew]);
   translate([-sw/2,-1,-3*ew/2-1])
   cube(size=[sw,sd+1,3*ew+2]);
  }
  // magnet
  translate([ew/2+magnet_offset,-clearance+epsilon,0])
  rotate([-90,0,0]) cylinder(d=5,h=1,$fn=5*fnd);
  // pcb with sensor
  translate([ew/2+magnet_offset-sensor_protrusion,-pcbc,0])
  rotate([0,90,0]) rotate([0,0,180]) {
   translate([-sensor_protrusion/4,sensor_offset,0])
   cube(size=[sensor_protrusion/2,1,sensor_protrusion]);
   linear_extrude(height=1)
   translate([-pcb_size.x/2,0])
   let(r=2) offset(r=+r,$fn=r*fnr) offset(r=-r)
   square(size=pcb_size);
  }
 }
 % vitamins();
 
 md = sd*2/3;
 th = max(screw_l-md+screw_hh,s);
 sod1 = pcb_mountholes_offset*2;
 sod2 = max( 2*(pcb_mountholes_offset-standoff_h),
             pcb_mountscrew_d+max(extrusion_width,layer_height) );
 pcbc = max(0,clearance-sensor_offset);

 difference() {
  union() {   
   cylinder(d=sw,h=pcb_size.x,center=true,$fn=sw*fnd);
   hull() {
    rotate([90,0,0])
    linear_extrude(height=th)
    let(r=sod1/2) offset(r=+r,$fn=r*fnr) offset(r=-r)
    square(size=[ew,pcb_size.x],center=true);
    translate([ew/2+magnet_offset-sensor_protrusion-standoff_h,-pcbc,0])
    rotate([180,0,0]) rotate([0,-90,0])
    linear_extrude(height=th)
    translate([-pcb_size.x/2,0])
    let(r=sod1/2) offset(r=+r,$fn=r*fnr) offset(r=-r)
    square(size=pcb_size);
   }//hull
   for(sz=[-1,1]) for(sy=[-1,1])
   translate([ew/2+magnet_offset-sensor_protrusion-standoff_h-epsilon,
              -pcbc-pcb_size.y/2+sy*(pcb_size.y/2-pcb_mountholes_offset),
              sz*(pcb_size.x/2-pcb_mountholes_offset)])
   rotate([0,90,0])
   cylinder(d1=sod1, d2=sod2,
            h=standoff_h+epsilon,
            $fn=pcb_mountholes_offset*2*fnd );
  }//union
  hull() for(sz=[-1,1]) translate([0,0,sz*tnut_l/2])
  rotate([-45,0,0]) translate([-sw/2-1,0,0])
  cube(size=[sw+2,sd,sd]);
  rotate([90,0,0]) {
   cylinder(d=screw_d,h=(th+1)*2,center=true,$fn=screw_d*fnd);
   translate([0,0,screw_l-md])
   cylinder(d=screw_hd,h=pcb_size.y+1,$fn=screw_hd*fnd);
  }
  for(sz=[-1,1]) for(sy=[-1,1])
  translate([ew/2+magnet_offset-sensor_protrusion-standoff_h-epsilon,
             -pcbc-pcb_size.y/2+sy*(pcb_size.y/2-pcb_mountholes_offset),
             sz*(pcb_size.x/2-pcb_mountholes_offset)])
  rotate([0,90,0])
  cylinder(d=pcb_mountscrew_d,h=(max(standoff_h,th)+1)*2,center=true,$fn=pcb_mountscrew_d*fnd);
  
  sx0=-ew/2; sx1=ew/2+magnet_offset-sensor_protrusion-standoff_h-th;
  sy0=-th; sy1=-pcbc-pcb_size.y;
  sx = abs(sx1-sx0); sy = abs(sy1-sy0);
  sxyr = min(sx,sy);
  * translate([sx1-sxyr,sy0-sxyr,0])
  cylinder(r=sxyr,h=pcb_size.x+2,center=true,$fn=sxyr*fnr);
  translate([sx1,sy0,-pcb_size.x/2-1]) mirror([1,1,0])
  linear_extrude(height=pcb_size.x+2)
  let(r=sxyr/2) offset(r=+r,$fn=r*fnr) offset(r=-r)
  square(size=[sx+r+1,sy+r+1]);
 }//difference
 
}

hallmount();