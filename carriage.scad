layer_height=.2; extrusion_width=.5;
epsilon=.01;

module carriage(
 rods_span = 40,						// separation between rods
 rodmount_d = 5,
 rodmount_clearance = 6,
 rodmount_angle = 45,
 rodmount_l = 7,
 rodmount_screw_l = 20, rodmount_screw_d = 3,
 rodmount_nut_d = 5.5/cos(30), rodmount_nut_h = 3,
 mount_screw_l = 10, mount_screw_d = 3.2,
 mount_screwhead_d = 6 +.2, mount_screwhead_h = 3,
 mount_screw_hold_r = 4,					// material around mount screws
 mount_depth = 7 -1,
 belt_offset = 9,						// belt offset from the bottom/outer side of carriage
 belts_apart = 12.22,						// the diameter of pulleys
 belt_thickness = 0.8, belt_tooth = 0.6, belt_pitch = 2,
 belt_w = 6,
 belt_chamfer = 1,
 rail_w = 20,							// rail width, basically carriage width
 l = 66,							// the length of the whole thing
 belt_clearance = .8,						// how much space to leave around the moving belt
 belt_thickness_tolerance = .1,
 belt_tooth_tolerance = .2
) {
 fnd=PI*2; fnr=fnd*2;
 long_pattern = [3.8*2,28.5*2];
 short_pattern = [20,20];
 h = max(2*rodmount_clearance,belt_offset+belt_w+belt_clearance+belt_chamfer);
 rodmount_offset = 0;// min(l/2-rodmount_clearance,long_pattern.y/2-mount_screwhead_d/2-rodmount_nut_d/2);
 clamps_y0 = -long_pattern.y/2+mount_screwhead_d/2;
 clamps_y1 = long_pattern.y/2-mount_screwhead_d/2;
 clamps_dy = clamps_y1-clamps_y0;
 clamps_x0 = -belts_apart/2+belt_tooth+belt_clearance;
 clamps_x1 = belts_apart/2;
 clamps_dx = clamps_x1-clamps_x0;
 clamps_walls = clamps_dx-belt_thickness;
 hold_w = clamps_walls*2/3;
 unhold_w = clamps_walls/3;
 belt_l = clamps_dy/2-belt_thickness-belt_tooth-belt_clearance;
 beltunnel = belts_apart+2*belt_thickness+belt_clearance;
 beltu0 = -belts_apart/2-belt_thickness-belt_clearance;
 
 module screwpattern(pattern) {
  for(sx=[-1,1]) for(sy=[-1,1]) translate([sx*pattern.x/2,sy*pattern.y/2,0]) children();
 }//screwpattern module
 module screwpatterns() {
  for(pattern=[long_pattern,short_pattern]) screwpattern(pattern) children();
 }//screwpatterns module
 module belt(pitch=belt_pitch,width=belt_w,thickness=belt_thickness+belt_thickness_tolerance,tooth=belt_tooth+belt_tooth_tolerance,l) {
  translate([-l/2,0,0])
  intersection() {
   union() {
    cube(size=[l,thickness,width]);
    for(x=[0:pitch:l/2]) for(sx=[-1,1])
     translate([l/2+sx*x,thickness,0]) cylinder(r=tooth/cos(30),h=width,$fn=6);
   }//union
   translate([0,-1,-1]) cube(size=[l,thickness+tooth+2,width+2]);
  }//intersection
 }//belt module
 %* for(mx=[0,1]) mirror([mx,0,0]) translate([belts_apart/2+belt_thickness,0,belt_offset]) rotate([0,0,90]) belt(l=l+2);
 
 difference() {
  union() {
   // rod mounts
   bigd = (rodmount_d+rods_span)*tan(rodmount_angle);
   intersection() {
    for(mx=[0,1]) mirror([mx,0,0])
     translate([0,rodmount_offset,rodmount_clearance]) rotate([0,90,0])
      cylinder(d1=bigd,d2=rodmount_d,h=rods_span/2,$fn=bigd*fnd);
    translate([-rods_span/2,-l/2,0])
    cube(size=[rods_span,l,h]);
   }
   // whole thing
   translate([0,-l/2,0]) hull() {
    translate([-rail_w/2,0,0])
    cube(size=[rail_w,l,belt_offset-belt_clearance]);
    translate([beltu0-extrusion_width,0,0])
    cube(size=[beltunnel+2*extrusion_width,l,h]);
   }
   // addition for screws
   screwpatterns()
   cylinder(r=mount_screw_hold_r,h=mount_screw_l-mount_depth-epsilon,$fn=mount_screw_hold_r*fnr);
  }//union
  difference() {
   translate([beltu0,-l/2-1,belt_offset-belt_clearance])
   cube(size=[beltunnel,l+2,h-belt_offset+2]);
   // belt clamps
   translate([0,(clamps_y0+clamps_y1)/2,0])
   for(my=[0,1]) mirror([0,my,0]) {
    difference() {
     hull() for(y=[hold_w/2+max(belt_thickness+belt_tooth+belt_clearance,rodmount_nut_d*cos(30)/2),clamps_dy/2-hold_w/2])
      translate([clamps_x1-hold_w/2,y,0]) {
       cylinder(d=hold_w,h=h-belt_chamfer,$fn=hold_w*fnd);
       cylinder(d=hold_w-2*belt_chamfer,h=h,$fn=hold_w*fnd);
      }//translate (for hull)
     translate([0,belt_thickness+belt_tooth+belt_clearance+belt_l/2,belt_offset-epsilon])
     let(l=belt_l,h=h-belt_offset+1) {
      translate([clamps_x1+belt_thickness,0,0]) rotate([0,0,90]) belt(l=l,width=h);
      translate([clamps_x0+unhold_w,0,0]) rotate([0,0,-90]) belt(l=l,width=h);
     }//let
    }//difference
    hull() for(y=[rodmount_nut_d*cos(30)/2+unhold_w/2,clamps_dy/2-unhold_w/2])
     translate([clamps_x0+unhold_w/2,y,0]) {
      cylinder(d=unhold_w,h=h-belt_chamfer,$fn=unhold_w*fnd);
      cylinder(d=unhold_w-2*belt_chamfer,h=h,$fn=unhold_w*fnd);
     }//translate (for hull)
   }//mirror for
  }//difference
  // rods mount channel
  translate([0,rodmount_offset,rodmount_clearance]) rotate([0,90,0]) {
   cylinder(d=rodmount_screw_d,h=rods_span+2,center=true,$fn=rodmount_screw_d*fnd);
   cylinder(d=rodmount_nut_d,h=rods_span+2*rodmount_l-2*rodmount_screw_l+2*rodmount_nut_h,center=true,$fn=6);
   hull() for(o=[0,h]) translate([-o,0,beltu0+epsilon]) cylinder(d=rodmount_nut_d,h=beltunnel-2*epsilon,$fn=6);
  }
  
  // mount screw holes
  screwpatterns() {
   translate([0,0,-1])
   cylinder(d=mount_screw_d,h=h+2,$fn=mount_screw_d*fnd);
   translate([0,0,mount_screw_l-mount_depth])
   cylinder(d=mount_screwhead_d,h=h+1,$fn=mount_screwhead_d*fnd);
  }
  // cut off excess
  cutty = clamps_y0;
  for(my=[0,1]) mirror([0,my,0])
  translate([0,cutty,h])
  rotate([atan((h-belt_offset+belt_clearance)/(cutty+l/2)),0,0])
  translate([-rail_w/2-1,-l/2,0])
  cube(size=[rail_w+2,l,h]);
 }//difference
 
}//carriage module

carriage();

/* The design (not the code) is mostly based on https://www.thingiverse.com/thing:566093/ */
