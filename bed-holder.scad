layer_height=.2; extrusion_width=.5;
epsilon=.01;

module the_holder(
 d_bed = 200,
 h_bed = 5.25,
 h_standoff = 18,
 d_standoff = 5.6/cos(30),
 delta_r = 4,                   // the distance from mounting hole to the bed
                                // circumference
 d_bscrew = 3.1,                // bed/standoff screw
 d_bscrewhead = 6, h_bscrewhead = 3,
 d_mscrew = 3.1,                // mount screw (to mount on extrusion)
 d_mscrewhead = 6,
 l_mscrew = 16,
 l_mscrewhole = 5,
 d_hscrew = 3.1,                // holder screw
 d_hscrewhead = 6, h_hscrewhead = 3,
 l_hscrew = 16,
 w_hnut = 5.4, h_hnut = 2.5,
 d_hwasher = 9.7,
 c_standoff_r = 2,              // standoff clearance (radius)
 c_bed_r = 1,                   // bed clearance (radius)
 c_bed_h = 3,                   // bed clearance
 c_hscrew_l = 3,                // glassfit screw l clearance, including washer and glassfit
 shell_z = 3,
 shell_xy = extrusion_width*3,
) {
 fnd = PI*2; fnr = 2*fnd;
 
 
 l_hscrewhole = l_hscrew-shell_z+c_hscrew_l;
 d_standoffer = d_standoff + c_standoff_r*2 + 2*shell_xy;
 d_glassfitter = max( max(d_hscrewhead,w_hnut) + 2*shell_xy, d_hwasher );
 y_glassfitter = -d_glassfitter/2 - max(c_bed_r,c_standoff_r+d_standoff/2-delta_r);
 h_standoffer = h_standoff + shell_z;
 h_glassfitter = max(
  shell_z + h_hnut + shell_z,
  l_hscrewhole-(d_glassfitter/2-d_hscrew/2) + 2*layer_height
 );
 delta_h = h_standoffer+h_bed-h_glassfitter;
 
 echo("glassfit r",d_glassfitter/2);
 echo("min hscrew l",shell_z+h_hnut);
 % union() {
  difference() {
   translate([0,d_bed/2,0]) cylinder(d=d_bed,h=h_bed);
   translate([0,delta_r,-1]) cylinder(d=d_bscrew,h=h_bed+2,$fn=12);
  }//difference
  translate([0,y_glassfitter,h_bed+1]) mirror([0,0,1]) cylinder(d=d_hscrew,h=l_hscrewhole+1,$fn=12);
 }//union %
 
 difference() {
  hull() {
   let(d=max( d_glassfitter-delta_h*2, d_hscrew+2*shell_xy ))
   translate([0,min(y_glassfitter-d_glassfitter/2+delta_h+d/2,delta_r),0]) mirror([0,0,1])
   cylinder(d=d,h=h_standoffer,$fn=d*fnd);
   translate([0,delta_r,h_bed]) mirror([0,0,1])
   cylinder(d=d_standoffer,h=h_standoffer+h_bed,$fn=d_standoffer*fnd);
   translate([0,y_glassfitter,h_bed]) mirror([0,0,1])
   cylinder(d=d_glassfitter,h=h_glassfitter,$fn=d_glassfitter*fnd);
  }
  // bed
  let(d=d_bed+2*c_bed_r)
  translate([0,d_bed/2,-c_bed_h])
  cylinder(d=d,h=h_bed+c_bed_h+1,$fn=d*fnd);
  // standoff
  translate([0,delta_r,0])
  let(d=d_standoff+2*c_standoff_r)
  translate([0,0,h_bed+1]) mirror([0,0,1]) {
   cylinder(d=d,h=h_standoff+h_bed+1,$fn=fnd*d);
   cylinder(d=d_bscrew,h=h_standoffer+h_bed+2,$fn=fnd*d_bscrew);
  }//translate standoff
  // glassfit
  translate([0,y_glassfitter,0]) {
   difference() {
    translate([0,0,h_bed+1]) mirror([0,0,1])
    cylinder(d=d_hscrew,h=l_hscrewhole+1,$fn=d_hscrew*fnd);
    translate([0,0,h_bed-shell_z])
    cylinder(d=d_hscrew+1,h=layer_height*3/2);
   }
   translate([-d_glassfitter/2-1,-w_hnut/2,h_bed-shell_z-h_hnut])
   cube(size=[d_glassfitter+2,w_hnut,h_hnut]);
  }
 }

}

the_holder();
