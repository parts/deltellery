layer_height=.2; extrusion_width=.48;
epsilon=.01;

side_delta_inner = 240+2*35.507 +1;
r_incircle = side_delta_inner/2/sqrt(3);
r_slot = r_incircle+20/2 +.5;
d_bed = 200;
r_bedscrews = d_bed/2-4;
delta_r = r_slot-r_bedscrews;
echo("∆r",delta_r);

module the_holder(
 d_piezo=20,            // and yes, my 27mm piezo has 28mm diameter
 delta_r = delta_r,     // offset from the extrusion slot to the mount hole
 h_flex = 0.5,
 w_psupport = 2,        // width of piezo support border
 w_ext = 20,            // extrusion width
 d_screw_mount = 3.1,   // extrusion mount
 d_screwhead_mount = 6,
 h_screwhead_mount = 3,
 l_screw_mount = 8,
 l_screwhole_mount = 5,
 l_tnut = 10,           // tnut length
 clearance_tnut = 2,    // tnut clearance
 d_screw_bed = 3.1,     // bed mont
 d_screwhead_bed = 6,
 h_screwhead_bed = 3,
 w_wires = 1.5,         // wires channel width
 h_wires = 1.5,           // wires channel height
 s_z = 2.5,
 //s_z_thin = max(3,28/5)*layer_height,
 s_xy_min = 2*extrusion_width,
 s_xy = 1.5,
 what="*", // extrusion|bed
 
 centerholed = false
) {
 fnd=PI*2; fnr=fnd*2;
 
 s_z_thin = max(3,d_piezo/6)*layer_height;
 echo("thin thickness",s_z_thin);
 d_pusher = d_screwhead_bed+2*s_xy_min;
 h_pusher = (h_screwhead_bed+s_z-s_z_thin)+1;
 h_mount = l_screw_mount-l_screwhole_mount;
 d_ppart = d_piezo + 2*s_xy;
 d_mpart = d_screwhead_mount + 2*s_xy;
 span_mount = max( w_ext, d_mpart, l_tnut+clearance_tnut,
  2*sqrt( pow((d_mpart+d_ppart)/2,2) - pow(delta_r,2) )
 );
 echo("span_mount",span_mount);
 y_piezo = delta_r;
 
 h_cfloor = s_z+h_screwhead_mount;
 
 h_floor = centerholed ? h_cfloor : s_z;

 module what_extrusion() {
  difference() {
   union() {
    hull() {
     for(mx=[0,1]) mirror([mx,0,0]) translate([span_mount/2,0,0]) {
      cylinder(d=w_ext,h=h_mount,$fn=w_ext*fnd);
      cylinder(d=d_screwhead_mount+2*s_xy,h=h_mount+h_screwhead_mount,$fn=(d_screwhead_mount+2*s_xy)*fnd);
     }
     translate([0,y_piezo,0])
     cylinder(d=d_ppart,h=h_mount+h_screwhead_mount,$fn=d_ppart*fnd);
    }
    translate([0,y_piezo,0])
    cylinder(d=d_ppart,h=h_floor+h_flex+h_pusher,$fn=d_ppart*fnd);
   }//union
   // extrusion_mount
   for(mx=[0,1]) mirror([mx,0,0]) translate([span_mount/2,0,0]) {
    translate([0,0,-1])
    cylinder(d=d_screw_mount,h=h_mount+2,$fn=d_screw_mount*fnd);
    translate([0,0,h_mount])
    cylinder(d=d_screwhead_mount,h=h_screwhead_mount+1,$fn=d_screwhead_mount*fnd);
   }
   // piezo compartment
   translate([0,y_piezo,0]) {
    translate([0,0,h_floor]) {
     cylinder(d=d_piezo-2*w_psupport,h=h_flex+h_pusher+1,$fn=(d_piezo-2*w_psupport)*fnd);
     translate([0,0,h_flex]) {
      cylinder(d=d_piezo,h=h_pusher+1,$fn=d_piezo*fnd);
      translate([-w_wires/2,0,0])
      cube(size=[w_wires,d_ppart/2+1,h_pusher+1]);
     }//translate(h_flex)
    }//translate(s_z)
   }//translate(y_piezo)
  }//difference
 }//what_extrusion module
 
 module what_centerholed() {
  translate([0,y_piezo,0])
  difference() {
   cylinder(d=d_ppart,h=h_floor+h_flex+h_pusher,$fn=d_ppart*fnd);
   translate([0,0,h_floor]) {
    cylinder(d=d_piezo-2*w_psupport,h=h_flex+h_pusher+1,$fn=(d_piezo-2*w_psupport)*fnd);
    translate([0,0,h_flex]) {
     cylinder(d=d_piezo,h=h_pusher+1,$fn=d_piezo*fnd);
     translate([-w_wires/2,0,epsilon])
     cube(size=[w_wires,d_ppart/2+1,h_pusher+1]);
    }//translate(h_flex)
    translate([0,0,-h_screwhead_mount])
    cylinder(d=d_screwhead_mount,h=h_screwhead_mount+1,$fn=d_screwhead_mount*fnd);
   }//translate(h_floor)
   translate([0,0,-1])
   cylinder(d=d_screw_mount,h=h_floor+2,$fn=d_screw_mount*fnd);
  }//difference
 }//what_centerholed module

 module what_bed() {
  translate([0,y_piezo,h_floor+h_flex+h_pusher]) {
   difference() {
    union() {
     cylinder(d=d_ppart,h=s_z_thin,$fn=d_ppart*fnd);
     mirror([0,0,1]) translate([0,0,-epsilon]) {
      difference() {
       union() {
        cylinder(d=d_piezo,h=h_pusher-h_wires-(s_xy-extrusion_width)+epsilon,$fn=d_piezo*fnd);
        translate([0,0,h_pusher-h_wires-(s_xy-extrusion_width)])
        cylinder(d1=d_piezo,d2=d_piezo-2*(s_xy-extrusion_width),h=s_xy-extrusion_width,$fn=d_piezo*fnd);
       }//union
       cylinder(d=d_piezo-2*s_xy,h=h_pusher+1,$fn=(d_piezo-2*s_xy)*fnd);
      }//difference
      cylinder(d=d_pusher,h=h_pusher,$fn=d_pusher*fnd);
     }//translate mirror
    }//union
    mirror([0,0,1]) {
     translate([0,0,-s_z_thin-1])
     cylinder(d=d_screw_bed,h=h_pusher+s_z_thin+2,$fn=d_screw_bed*fnd);
     translate([0,0,s_z-s_z_thin])
     cylinder(d=d_screwhead_bed,h=h_pusher+1,$fn=d_screwhead_bed*fnd);
    }//mirror
   }//difference
  }//translate(y_piezo,z)
 }
 
 if(what=="extrusion") color("red",0.8) what_extrusion();
 else if(what=="centerholed") color("red",0.8) what_centerholed();
 else if(what=="bed") color("green",0.8) what_bed();
 else {
  color("red",0.8)
  if(centerholed) what_centerholed();
  else what_extrusion();
  color("green",0.8) what_bed();
 }
}

debug = true;

difference() {
 the_holder(what="*", d_piezo = 28, centerholed = true);
 if(debug) translate([0,-50,-50]) cube(size=[100,100,100]);
}

* for(i=[1:2]) for(j=[1:2]) translate([i*33,j*33,0])
the_holder(what="bed", d_piezo = 28, centerholed = true);
