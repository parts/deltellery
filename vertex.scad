layer_height=0.2;
epsilon=.01;

nema17_mounting_holes_span = 31;
nema17_mounting_holes_d = 3;
nema17_side = 42.3;
motor_bore_l = 24;
min_shell = 0.7535;

module vertex(
 extrusion_side = 20,           // extrusion dimensions
 extrusion_slot_depth = 6,
 extrusion_slot_width = 6,
 sides_mingap =                 // the minimum distance          
  nema17_mounting_holes_span    // between side extrusions
  +nema17_mounting_holes_d+2*min_shell,
 S=5,                           // thick shell
 lscrew=8, screw_d=3,           // the length of the screw to mount on extrusion
 screwhead_h=3, screwhead_d=6,  // screw head dimensions
 column_to_belt=15+3,           // distance from extrusion to timing belt
 belt_width = 6,
 idler_w = 8.5, idler_d = 12,   // idler bearing(s) dimensions
 idler_clearance = 2,
 tensioner_screw_d = 4,         // tensioner screw dimensions
 tensioner_screw_l = 30,
 tensioner_screwhead_d = 7,     // tensioner screw head dimensions
 tensioner_screwhead_h = 4,
 tensioner_nut_w = 7,           // tensioner nut dimensions
 tensioner_nut_h = 3.15,
 motor_bore_l = motor_bore_l,
 tnut_width=6, tnut_length=10,  // t-nut dimensions
 extrusions_gap=10,             // the distance between horizontal extrusions
 motor_mounting_holes_span = nema17_mounting_holes_span,
 motor_mounting_holes_d = 3,
 motor_hole_d = 22,
 motor_bearing_od = 16,         // motor support bearing dimensions
 motor_bearing_h = 5,
 sslightly=0.8,                 // how slightly to sink mounting screws
 motor_voffset=                 // motor vertical offset (wish I remembered why)
  10/2+10+3+3/2-31/2,
 motor_bearing_tolerance=.3,    // lotsa tolerances
 motor_hole_tolerance=1.5,
 motor_keyhole_tolerance=.5,
 extrusion_tolerance=.1,
 mountscrew_tolerance=.15,
 idler_slop=1,
 tensioner_screw_d_tolerance=.3,
 tensioner_nut_h_tolerance = .35,
 tensioner_nut_w_tolerance = .25,
 angled=true,                   // whether to make the corner
                                // with rounded angles or circular
 which="motor" // motor|idler|tensioner
) {

 module teardrop(d,h,rotate=0) {
  cylinder(d=d,h=h,$fn=2*PI*d);
  rotate([0,0,45+rotate]) cube(size=[d/2,d/2,h]);
 }

 module mirrors() {
  for(mx=[0,1]) mirror([mx,0,0]) for(mz=[0,1]) mirror([0,0,mz]) children();
 }
 module mirrorx() {
  for(mx=[0,1]) mirror([mx,0,0]) children();
 }
 module extrusion_profile() {
  offset(r=extrusion_tolerance) difference() {
   square(size=[extrusion_side,extrusion_side],center=true);
   md=min(extrusion_slot_width,extrusion_slot_depth);
   for(rz=[0,90]) rotate([0,0,rz])
   for(my=[0,1]) mirror([0,my])
   translate([0,extrusion_side/2])
   circle(d=md,$fn=2*PI*md);
  }
 }
 
 sides_gap = max(sides_mingap,extrusion_side+2*S);
 osides_gap = sides_gap+2*extrusion_side*sin(60);
 echo("sides gap",sides_gap,"outer sides gap",osides_gap);
 v_radius = osides_gap/(2*sin(60));
 echo("v radius",v_radius);
 column_back_offset = v_radius-min(S+screwhead_h,lscrew+extrusion_slot_depth);
 column_screw_head_offset = v_radius-screwhead_h;
 echo("column back offset",column_back_offset,
      "column screw head offset",column_screw_head_offset);
 sides_offset=[v_radius*sin(60),v_radius*cos(60)];
 //motor_offset=-column_back_offset+extrusion_side+column_to_belt+belt_width/2+motor_bore_l/2;
 motor_offset=-column_back_offset+extrusion_side+S +S+motor_bearing_h/2+motor_bore_l;
 echo("motor offset",motor_offset);
 wing_l = motor_offset/sin(30)+S+tnut_length;
 side_holes = [tnut_length/2+extrusion_slot_width/2,wing_l-tnut_length/2];
 midbelt_y = -column_back_offset+extrusion_side+column_to_belt+belt_width/2;
 
 module vertex_profile() {
 corner_d=S; corr=corner_d/2-corner_d*sin(30)/2;
 difference() {
   union() {
    // circle (or "circle")
    if(angled)
     let(r=corner_d)
     offset(r=r,$fn=30) offset(r=-r,$fn=30)
     circle(r=v_radius/cos(30),$fn=6);
    else
     circle(r=v_radius,$fn=2*v_radius*2*PI);
    // body
    translate([-osides_gap/2,-sides_offset.y])
    square(size=[osides_gap,sides_offset.y+motor_offset+corner_d+1]);
    // wings
    for(mx=[0,1]) mirror([mx,0])
    translate([sides_offset.x,-sides_offset.y])
    rotate([0,0,-30])
    if(true/*XXX:no rounded wings ends*/)
     translate([-extrusion_side-S,0]) square([S,wing_l]);
    else
     hull() for(y=[0,wing_l])
     translate([-extrusion_side-S/2,y]) circle(d=S,$fn=2*PI*S);
   }//union
   
   translate([0,-column_back_offset+extrusion_side/2])
   extrusion_profile();
   // cut of excess for side extrusions
   for(mx=[0,1]) mirror([mx,0])
   translate([sides_offset.x,-sides_offset.y])
   rotate([0,0,-30])
   translate([-extrusion_side,0]) square(size=[extrusion_side,(sides_offset.y+motor_offset+corner_d+2)/cos(30)]);

   for(oyoy=[
     [column_back_offset-extrusion_side-S+corr,-motor_offset+S+corr+corner_d],
     [-motor_offset+corr,-motor_offset+corr+corner_d-wing_l/cos(30)]
   ])
    hull() for(mx=[0,1]) mirror([mx,0])
     for(oy=oyoy)
      translate([sides_offset.x,-sides_offset.y])
      rotate([0,0,60])
      translate([(sides_offset.y-extrusion_side*sin(30)-oy)/cos(30),extrusion_side+S+corner_d/2])
      circle(d=corner_d,$fn=2*PI*S);
  }//difference
 }//module vertex_profile
 
 module tnut_cutout() {
  translate([0,0,-epsilon]) hull() for(sx=[-1,1])
  translate([sx*tnut_length/2,-extrusion_slot_width/2-1,0])
  rotate([0,-45,0])
  cube(size=[2*extrusion_slot_depth,extrusion_slot_width+2,2*extrusion_slot_depth]);
 }

 // the whole thing height
 vheight = (which=="motor")
  ? extrusion_side*2 + extrusions_gap
  : extrusion_side;
 // Z-s of the rails
 railzz = (which=="motor")
  ? [for(z=[-1,1]) z*(extrusion_side/2+extrusions_gap/2)]
  : [0];

 // idler support cone heights
 ho = abs((-column_back_offset+extrusion_side+S-epsilon)-(midbelt_y-idler_w/2-idler_slop/2));
 hi = abs((motor_offset-S+epsilon)-(midbelt_y+idler_w/2+idler_slop/2));
 
 if(which=="idler" || which=="tensioner") {
  // show idler
  % translate([0,midbelt_y,0]) rotate([90,0,0]) cylinder(d=idler_d,h=idler_w,center=true);
 }

 idler_s = S;
 tensioner_w = idler_w+2*idler_s;
 tensioner_screw_s = 1.5; // TODO: move upstairs
 tensioner_top_d = tensioner_screw_d+2*tensioner_screw_s;
 tensioner_w = idler_w+idler_slop + 2*S;
 echo("tensioner width",tensioner_w);
 tensioner_l = idler_d/2+idler_clearance+tensioner_screw_l
  -S -(tensioner_w/2-tensioner_top_d/2) -idler_d/2 ;
 idler_travel = tensioner_l - idler_d/2-idler_clearance;
 echo("idler travel",idler_travel);

 module tensioner() {
  idler_d6 = idler_d/cos(30);
  l = tensioner_l;
  w = tensioner_w;
  // tensioner screw
  % translate([0,midbelt_y,l+idler_d/2+idler_clearance+w/2-tensioner_top_d/2+2*S])
  mirror([0,0,1]) {
   cylinder(d=tensioner_screw_d,h=tensioner_screw_l);
   mirror([0,0,1]) cylinder(d=tensioner_screwhead_d,h=tensioner_screwhead_h);
  }
  difference() {
   union() {
    // sides
    for(sy=[-1,1]) hull() {
     translate([0,midbelt_y+sy*(w/2-S),0]) rotate([-sy*90,0,0])
     rotate([0,0,30]) cylinder(d=idler_d6,h=S,$fn=6);
     translate([0,midbelt_y+sy*(w-S)/2,l])
     cube(size=[idler_d,S,idler_d],center=true);
    }
    // top
    hull() {
     translate([0,midbelt_y,l])
     cube(size=[idler_d,w,idler_d],center=true);
     translate([0,midbelt_y,l+(idler_d+w-tensioner_top_d)/4])
     cube(size=[idler_d,tensioner_top_d,(idler_d+w-tensioner_top_d)/2],center=true);
    }//hull
   }//union
   // tensioner screw
   translate([0,midbelt_y,l])
   cylinder(d=tensioner_screw_d+tensioner_screw_d_tolerance,h=w+idler_d+2,center=true,$fn=tensioner_screw_d*PI*2);
   // tensioner nut pocket
   translate([0,midbelt_y,l])
   cube(size=[idler_d+2,tensioner_nut_w+tensioner_nut_w_tolerance,tensioner_nut_h+tensioner_nut_h_tolerance],center=true);
   // TODO: move definitions upstairs
   idler_screw_d = screw_d;
   idler_screwhead_d = screwhead_d;
   idler_screwhead_h = screwhead_h;
   idler_nut_w = 5.4;
   idler_nut_d = idler_nut_w/cos(30);
   idler_nut_h = 2.5;
   idler_screw_d_tolerance = .3;
   idler_nut_h_tolerance = .4;
   idler_nut_d_tolerance = .3;
   idler_screwhead_d_tolerance = .1;
   idler_screwhead_h_tolerance = .2;
   translate([0,midbelt_y,0]) rotate([90,0,0])
   // idler screw hole
   cylinder(d=idler_screw_d+idler_screw_d_tolerance,h=w+2,center=true,$fn=2*PI*idler_screw_d);
   // screwhead hole
   translate([0,midbelt_y+w/2,0])
   rotate([90,0,0])
   translate([0,0,-1])
   cylinder(d=idler_screwhead_d+idler_screwhead_d_tolerance,h=idler_screwhead_h+idler_screwhead_h_tolerance+1,$fn=2*PI*idler_screwhead_d);
   // nut hole
   translate([0,midbelt_y-w/2,0])
   rotate([-90,0,0])
   translate([0,0,-1])
   cylinder(d=idler_nut_d+idler_nut_d_tolerance,h=idler_nut_h+idler_nut_h_tolerance+1,$fn=6);
  }//difference  
 }//tensioner module

 if(which=="tensioner") {
  tensioner();
 }else{
  if(which=="idler") % tensioner();
  difference() {
   union() {
    
    translate([0,0,-vheight/2])
    linear_extrude(height=vheight,convexity=10)
    vertex_profile();
    
    // sides guides
    d=min(2*extrusion_slot_depth,extrusion_slot_width);
    mirrorx() {
     for(z=railzz) translate([0,0,z])
     rotate([0,0,30]) rotate([-90,0,0])
     translate([-v_radius+extrusion_side,0,0]) difference() {
      cylinder(d=d,h=wing_l,$fn=2*PI*d);
      for(o=side_holes)
       translate([0,0,o]) rotate([0,-90,0]) tnut_cutout();
     }//difference
    }//mirrorx
    
    if(which=="motor") {
     // motor support bearing socket
     bho=column_back_offset-extrusion_side-motor_offset+motor_bore_l-S;
     echo(bho);
     translate([0,-column_back_offset+extrusion_side+S-epsilon,motor_voffset])
     rotate([-90,0,0]) difference() {
      cylinder(d1=motor_bearing_od+motor_bearing_tolerance+S+bho,
               d2=motor_bearing_od+motor_bearing_tolerance+S,
               h=bho,
               $fn=2*PI*(motor_bearing_od+motor_bearing_tolerance+S+bho));
      translate([0,0,bho-motor_bearing_h])
      cylinder(d=motor_bearing_od,h=motor_bearing_h+1,$fn=2*PI*motor_bearing_od);
     }//difference
    }//if(motor)
 
    if(which=="idler") {
     // tensioner
     side45 = tensioner_w/2-tensioner_top_d/2+S;
     tensioner_bracket_top = idler_d/2+idler_clearance+tensioner_screw_l
      + idler_travel/2;
     difference() {
      union() {
       // inner wall
       translate([-extrusion_side/2,motor_offset-S,0])
       cube(size=[extrusion_side,S,tensioner_bracket_top-S]);
       // outer wall
       translate([-extrusion_side/2,-column_back_offset+extrusion_side,0])
       cube(size=[extrusion_side,S,tensioner_bracket_top-S]);
       // roof
       translate([-extrusion_side/2,-column_back_offset+extrusion_side+S,tensioner_bracket_top-S])
       cube(size=[extrusion_side,column_back_offset-extrusion_side+motor_offset-2*S,S]);
       // inner corner
       hull() {
	    translate([-extrusion_side/2,motor_offset-S,tensioner_bracket_top-side45])
	    cube(size=[extrusion_side,S,side45-S]);
	    translate([-extrusion_side/2,motor_offset-side45,tensioner_bracket_top-S])
	    cube(size=[extrusion_side,side45-S,S]);
       }
       // outer corner
       hull() {
	translate([-extrusion_side/2,-column_back_offset+extrusion_side,tensioner_bracket_top-side45])
	cube(size=[extrusion_side,S,side45-S]);
	translate([-extrusion_side/2,-column_back_offset+extrusion_side+S,tensioner_bracket_top-S])
	cube(size=[extrusion_side,side45-S,S]);
       }
      }//union
      hull() for(y=[
        -column_back_offset+extrusion_side+side45+(tensioner_screw_d+tensioner_screw_d_tolerance)/2,
        motor_offset-side45-(tensioner_screw_d+tensioner_screw_d_tolerance)/2
       ])
        translate([0,y,tensioner_bracket_top-S+layer_height])
        cylinder(d=tensioner_screw_d+tensioner_screw_d_tolerance,h=S+1,$fn=PI*tensioner_screw_d*2);
     }//difference
    }//if(idler)
    
   }//union
   
   if(which=="motor") {
    // motor mounting holes
    for(xm=[0,1]) mirror([xm,0,0])
    for(zo=[-motor_mounting_holes_span/2,motor_mounting_holes_span/2])
    translate([motor_mounting_holes_span/2,0,zo+motor_voffset]) {
    rotate([-90,0,0]) cylinder(d=motor_mounting_holes_d+2*mountscrew_tolerance,h=motor_offset+1,$fn=2*PI*motor_mounting_holes_d);
    rotate([90,0,0])
    cylinder(d=motor_mounting_holes_d+motor_keyhole_tolerance,h=v_radius/cos(30)+1,$fn=2*PI*motor_mounting_holes_d);
    }//translate
    // motor hole
    translate([0,0,motor_voffset])
    rotate([-90,0,0])
    teardrop(d=motor_hole_d+motor_hole_tolerance,h=motor_offset+1,rotate=180);
    //show motor
    %translate([0,motor_offset+1,motor_voffset])
    cube(size=[nema17_side,1,nema17_side],center=true);
   }//if(motor)
 
   // column mounting holes
   for(z=railzz)
   translate([0,-column_back_offset+extrusion_side/2,z])
   rotate([90,0,0]) {
    cylinder(d=screw_d+2*mountscrew_tolerance,h=v_radius,$fn=2*PI*screw_d);
    translate([0,0,extrusion_side/2+lscrew-extrusion_slot_depth/2])
    cylinder(d=screwhead_d,h=v_radius,$fn=2*PI*screwhead_d);
    translate([0,0,extrusion_side/2]) rotate([0,0,90]) mirror([0,0,1]) tnut_cutout();
   }//rotate

   mirrorx() for(z=railzz) {
    // sides mounting holes
    for(o=side_holes)
     rotate([0,0,60])
     rotate([90,0,0])
     translate([o,z,v_radius-extrusion_side])
     translate([0,0,-S-3]) {
      cylinder(d=screw_d+2*mountscrew_tolerance,h=S+6,$fn=2*PI*screw_d);
      cylinder(d=screwhead_d,h=sslightly+3,$fn=2*PI*screwhead_d);
     }//translate
    if(which=="motor") {
     // the void within
     hull()
     for(oy=[0,-tnut_length*3/4])
     for(oz=[min(railzz)+extrusion_side/2,max(railzz)-extrusion_side/2])
     rotate([0,0,-30])
     translate([v_radius-extrusion_side/2,oy,oz])
     rotate([0,90,0])
     cylinder(d=tnut_length,h=tnut_length,center=true,$fn=2*PI*tnut_length);
    }//if(motor)
    //tnut slots
    for(zz=[[min(railzz),1],[max(railzz),0]])
    translate([0,0,zz[0]]) mirror([0,0,zz[1]])
    hull() for(oy=[0,-tnut_length])
    rotate([0,0,-30])
    translate([v_radius-extrusion_side/2,oy,extrusion_side/2-extrusion_slot_depth])
    cylinder(d=tnut_length,h=extrusion_slot_depth+1,$fn=2*PI*tnut_length);
   }//for(railzz)

  }//difference

  // adhesion pads
  * hull() mirrorx()
  rotate([0,0,-30])
  translate([v_radius-extrusion_side-S/2,wing_l,-vheight/2])
  cylinder(d=5*S,h=layer_height);
 }

}

vertex(which="motor");
